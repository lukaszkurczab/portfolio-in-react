import React, { useState } from 'react'
import InnerNavigationItem from '../InnerNavigationItem'
import ExternalNavigationItem from '../ExternalNavigationItem'

import logoImage from '../../assets/logo.png';
import twitterImage from '../../assets/twitter.png';
import facebookImage from '../../assets/facebook.png';
import linkedinImage from '../../assets/linkedin.png';

const Header = ({ refAboutMe, refSkills, refPortfolio, refBlog, refContact }) => {
  const [active, setActive] = useState(false)

  return (
    <header className="header">
      <div className="logo_wrapper">
        <img src={logoImage} alt="Łukasz Kurczab logo" className="logo_img" />
      </div>

      <div className={active ? "nav_wrapper show" : "nav_wrapper"}>
        <div className="navLinks_wrapper">
          <nav id="nav" className="nav_inner nav">
            <ul className="nav_list">
              <InnerNavigationItem nav={refAboutMe} text="About me" />
              <InnerNavigationItem nav={refSkills} text="Skills" />
              <InnerNavigationItem nav={refPortfolio} text="Portfolio" />
              <InnerNavigationItem nav={refBlog} text="Blog" />
              <InnerNavigationItem nav={refContact} text="Contact me" />
            </ul>
          </nav>

          <nav className="nav_list_wrapper nav_outside">
            <ul className="nav_list">
              <ExternalNavigationItem link="https://twitter.com/" src={twitterImage} alt="Twitter" />
              <ExternalNavigationItem link="https://www.facebook.com/kurczab.lukasz" src={facebookImage} alt="Facebook" />
              <ExternalNavigationItem link="https://www.linkedin.com/in/lukaszkurczab/" src={linkedinImage} alt="Linkedin" />
            </ul>
          </nav>
        </div>

        <div className="nav_btn" onClick={() => setActive(!active)}>
          <span className="nav_bars topBar"></span>
          <span className="nav_bars midBar"></span>
          <span className="nav_bars botBar"></span>
        </div>
      </div>
    </header>
  )
}

export default Header