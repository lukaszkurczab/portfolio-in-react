import React from 'react'

const ExternalNavigationItem = ({ link, src, alt }) => {
  return (
    <>
      <li className="nav_item">
        <a href={link} target="_blank" rel="noreferrer">
          <img src={src} alt={alt} className="nav_logo" />
        </a>
      </li>
    </>
  )
}

export default ExternalNavigationItem