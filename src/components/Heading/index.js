import React from 'react'

const Heading = ({ main, secondary }) => {
  return (
    <>
      <h2 className="mainHeading">{main}</h2>
      <h3 className="secondaryHeading">{secondary}</h3>
    </>
  )
}

export default Heading