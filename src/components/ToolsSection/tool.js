import React from 'react'

const Tool = ({ name, version, src }) => {
  return (
    <div className="tool_wrapper">
      <div className="toolBox">
        <img src={src} alt={name} className="toolLogo" />
        <p className="text">{name}</p>
        <p className="text">{version}</p>
      </div>
    </div>
  )
}

export default Tool