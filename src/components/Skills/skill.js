import React from 'react'
import styled from 'styled-components'

const Skills = ({ skill, percent }) => {
  const Wrapper = styled.div`
  width: ${percent};
  `

  return (
    <div className="skillBg">
      <Wrapper>
        <div className="skillBar php element">{skill} {percent}</div>
      </Wrapper>
    </div>
  )
}

export default Skills