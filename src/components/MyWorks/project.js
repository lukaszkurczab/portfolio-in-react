import React from 'react'
import LinkBox from '../LinkBox'

const Project = ({ src, alt, heading, type, tech, gitLink, projectLink }) => {
  return (
    <div className="project_wrapper">
      <div className="projectBox">
        <img src={src} alt={alt} className="projectImg" />
        <LinkBox gitLink={gitLink} projectLink={projectLink} />
        <div className="descBox">
          <h3 className="projHeading">{heading}</h3>
          <p className="projDesc">- {type}</p>
          <p className="projDesc">{tech}</p>
        </div>
      </div>
    </div>
  )
}

export default Project