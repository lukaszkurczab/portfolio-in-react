import React from 'react'

import GitImg from '../../assets/github_icon.png'
import NavigationImg from '../../assets/external_link_icon.png'

const LinkBox = ({ gitLink, projectLink }) => {
  return (
    <div className="buttonSmall__wrapper">
      <a href={gitLink} target="_blank" rel="noreferrer">
        <img src={GitImg} alt="GitHub" className="buttonSmall" />
      </a>
      <a href={projectLink}>
        <img src={NavigationImg} alt="Project" className="buttonSmall" />
      </a>
    </div>
  )
}

export default LinkBox