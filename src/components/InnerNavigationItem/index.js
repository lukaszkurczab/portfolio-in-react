import React from 'react'

const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop)

const InnerNavigationItem = ({ nav, text }) => {
  return (
    <>
      <li className="nav_item"><span onClick={() => scrollToRef(nav)} className="navLink">{text}</span></li>
    </>
  )
}

export default InnerNavigationItem