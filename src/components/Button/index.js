import React from 'react'

const Button = ({ text }) => {
  return (
    <>
      <button type="submit" className="buttonBig">{text}</button>
    </>
  )
}

export default Button