import React, { useRef } from 'react'
import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

const Homepage = () => {
  const refAboutMe = useRef(null)
  const refSkills = useRef(null)
  const refPortfolio = useRef(null)
  const refBlog = useRef(null)
  const refContact = useRef(null)

  return (
    <>
      <Header {...{ refAboutMe, refSkills, refPortfolio, refBlog, refContact }} />
      <Main {...{ refAboutMe, refSkills, refPortfolio, refBlog }} />
      <Footer {...{ refContact }} />
    </>
  )
}

export default Homepage